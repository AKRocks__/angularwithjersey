import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  heightOfPage: string;
  topPadding: string;
  leftPadding: string;
  widthOfPage: number;
  smallHeight: string;
  res;

  constructor(private http: HttpClient, private route: Router) {
    this.heightOfPage = (window.innerHeight.toString()).concat('px');
    this.topPadding = ((window.innerHeight / 5).toString()).concat('px');
    this.leftPadding = ((window.innerWidth / 30).toString()).concat('px');
    this.widthOfPage = window.innerWidth;
    this.smallHeight = ((window.innerHeight / 4).toString()).concat('px');
  }

  getSize(val){
    this.heightOfPage = (val.target.innerHeight.toString()).concat('px');
    this.topPadding = ((val.target.innerHeight / 5).toString()).concat('px');
    this.leftPadding = ((val.target.innerWidth / 30).toString()).concat('px');
    this.widthOfPage = val.target.innerWidth;
    this.smallHeight = ((val.target.innerHeight / 4).toString()).concat('px');
  }

  isSmall(){
    return (this.widthOfPage < 768);
  }

  getUrl(path: string, userId: string, password: string) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const url = ('http://localhost:8080/Tarkshala_Scholars_Engine_war/'.concat(path));
    const data = {username : userId, secret : password};

    this.http.post(url, JSON.stringify(data), options).subscribe(res => {
      console.log('Got');
      console.log(res);
      this.res = res;
    }, error => {
      console.log(error);
    }, () => {
      console.log('Completed');
      this.route.navigateByUrl('homepage',{state: {data:this.res}});
    });
  }
}
