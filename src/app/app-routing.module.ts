import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './mainComponents/login';
import {MainpageComponent} from './mainComponents/mainpage';
import {HomepageComponent} from './mainComponents/homepage';
import {AttendenceComponent} from './mainComponents/homepage/attendence';
import {PerformanceComponent} from './mainComponents/homepage/performance';
import {FeesComponent} from './mainComponents/homepage/fees';
import {ForgetPasswordComponent} from './mainComponents/forget-password';


const routes: Routes = [
  {path: '', component: MainpageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'login/forget-password', component: ForgetPasswordComponent},
  {path: 'homepage', component: HomepageComponent},
  {path: 'homepage/attendence', component: AttendenceComponent},
  {path: 'homepage/performance', component: PerformanceComponent},
  {path: 'homepage/fees', component: FeesComponent},
  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
