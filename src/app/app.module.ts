import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './mainComponents/login';
import { MainpageComponent } from './mainComponents/mainpage';
import {RouterModule} from '@angular/router';
import { HomepageComponent } from './mainComponents/homepage';
import { AttendenceComponent } from './mainComponents/homepage/attendence';
import { PerformanceComponent } from './mainComponents/homepage/performance';
import { FeesComponent } from './mainComponents/homepage/fees';
import { ForgetPasswordComponent } from './mainComponents/forget-password';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainpageComponent,
    HomepageComponent,
    AttendenceComponent,
    PerformanceComponent,
    FeesComponent,
    ForgetPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
